import { Platform } from 'react-native';
import { createMaterialTopTabNavigator,
         createAppContainer } from 'react-navigation';

import TemperatureNode from './TabScreens/TemperatureNode';
import GasNode from './TabScreens/GasNode';
import Dashboard from './TabScreens/Dashboard';
import MotionNode from './TabScreens/MotionNode';
import VibrationNode from './TabScreens/VibrationNode';
import DoorNode from './TabScreens/DoorNode';
import WifiConfiguration from './TabScreens/WifiConfiguration';

const TabNavigator = createMaterialTopTabNavigator({
    Dashboard: Dashboard,
    Temperature: TemperatureNode,
    Gas: GasNode,
    Motion: MotionNode,
    Vibration: VibrationNode,
    Door: DoorNode,
    Wifi: WifiConfiguration,
    
},
{
    swipeEnabled: true,
},
{
    tabBarOptions: {
        activeTintColor: '#8630dc',
        style: { marginTop: Platform.OS == 'ios' ? 20 : 24, 
        backgroundColor: '#e93766' },
        inactiveTintColor: '#dc3071',
        scrollEnabled: true,
    },
});

export default createAppContainer(TabNavigator);