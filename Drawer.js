import { createDrawerNavigator, 
         createAppContainer } from 'react-navigation';

import MyHomeScreen from './DrawerScreens/MyHomeScreen';
import NotificationsScreen from './DrawerScreens/NotificationsScreen';

const DrawerNavigator = createDrawerNavigator({
    Home:{
        screen: MyHomeScreen,
    },
    Settings: {
        screen: NotificationsScreen,
    },
});

export default createAppContainer(DrawerNavigator);