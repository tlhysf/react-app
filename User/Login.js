import React from 'react';
import { StyleSheet, 
        Text, 
        TextInput, 
        View,
        Image, 
        Alert,
        BackHandler,
        Button } from 'react-native';
import Firebase from 'firebase';
// import PasswordTextBox from './PasswordTextBox';

export default class Login extends React.Component{
    state = { username: '', email: '', password: '', errorMessage: null }
    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.backHandler.remove();
    }
    
    handleBackButtonClick = () => {
        Alert.alert(
            'Exit Application','Do you want to exit the application?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => BackHandler.exitApp()}
            ], 
            {cancelable: false}
        );
        // this.goBack();
        return true;
    }
    handleLogin = () => {
        Firebase.auth().signInWithEmailAndPassword(this.state.email.trim(), this.state.password)
        .then((user) => {
            Firebase.database().ref('/users/' + user.user.uid ).child('/isAdmin').on('value', snapshot => {
                let isAdmin = snapshot.val();
                if( isAdmin == true ){
                    console.log(isAdmin);
                    this.props.navigation.navigate('adminpage');
                } else if( isAdmin == false ) {
                    console.log(isAdmin);
                    // Firebase.auth().signOut();
                    this.props.navigation.navigate('Tabs')
                    // alert('You are not an Admin.');
                } else {
                    console.log(isAdmin);
                    Firebase.auth().signOut();
                    alert('Sorry Something went wrong, please try again.')
                }
            })
        }) 
        .catch(error => this.setState({ errorMessage: error.message}))
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('C:/Users/care111/Desktop/REACT-NATIVE PROJECTS/iotsecurity/images/atomic-energy.png')} />
                <Text style={styles.loginstyle}>Login</Text>
                    {this.state.errorMessage && 
                        <Text style={{color: 'red'}}>
                            {this.state.errorMessage}
                        </Text>}
                <TextInput style={styles.textInput} autoCapitalize='none' autoCorrect={false} placeholder='E-mail' 
                onChangeText={ email => this.setState({email}) } value={this.state.email} />
                <TextInput secureTextEntry style={styles.textInput} autoCapitalize='none' autoCorrect={false} placeholder='Password' 
                onChangeText={ password => this.setState({password}) } value={this.state.password} />
                {/* <PasswordTextBox icon='lock' placeholder='Password' style={styles.textInput} 
                onChangeText={ password => this.setState({password}) } value={this.state.password}/> */}
                <Button title='Login' color='#e93766' onPress={this.handleLogin} />
                <View>
                    <Text> Don't have an account? <Text onPress = {() => this.props.navigation.navigate('SignUp')} 
                    style = {styles.createaccount}>Sign Up</Text> </Text>
                </View>
                <View>
                    <Text> Forgot Password? <Text onPress = {() => this.props.navigation.navigate('ForgotPassword')} 
                    style = {styles.createaccount}>Forgot Password</Text> </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        height: 40,
        fontSize: 20,
        width: '90%',
        borderColor: '#9b9b9b',
        borderBottomWidth: 1,
        marginTop: 8,
        marginVertical: 15
    },
    loginstyle: {
        color: '#e93766',
        fontSize: 40
    },
    createaccount: {
        color: '#e93766',
        fontSize: 18
    }
})