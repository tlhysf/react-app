import React from 'react';
import { StyleSheet, 
        View, 
        Button, 
        TextInput,
        Image, 
        Text,
        BackHandler} from 'react-native';
import Firebase from 'firebase';

export default class ForgotPasword extends React.Component {
    state = { email: '', errorMessage: null}

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.backHandler.remove();
    }
    
    handleBackButtonClick = () => {
        this.props.navigation.navigate('Login');
        // this.goBack();
        return true;
    }

    handleForgot = () => {
        Firebase.auth().sendPasswordResetEmail(this.state.email.trim())
        .then( () => this.props.navigation.navigate('Login'))
        .catch(error => this.setState({errorMessage: error.message}))
    }

    render() {
        return (
            <View style = {styles.container}>
                <Image source={require('C:/Users/care111/Desktop/REACT-NATIVE PROJECTS/iotsecurity/images/atomic-energy.png')} />
                <Text style = {styles.signupstyle}>Forgot Password</Text>
                    {this.state.errorMessage && 
                        <Text style={{color: 'red'}}>
                            {this.state.errorMessage}
                        </Text>}
                    <TextInput placeholder='E-mail' autoCapitalize='none' autoCorrect={false} style={styles.textInput} 
                    onChangeText={email => this.setState({email})} value={this.state.email}/> 
                    <Button title='Confirm' color='#e93766' onPress = {this.handleForgot} />
                    <View>
                        <Text> Remembered Your Password? <Text onPress ={() => this.props.navigation.navigate('Login')} 
                        style={styles.alreadyaccount} > Login </Text> </Text>
                    </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
    },
    textInput: {
        height: 40,
        fontSize: 20,
        width: '90%',
        borderColor: '#0b9b9b',
        borderBottomWidth: 1,
        marginTop: 8,
        marginVertical: 15
    },
    signupstyle: {
        color: '#e93766',
        fontSize: 40
    },
    alreadyaccount: {
        color: '#e93766',
        fontSize: 18
    }
})