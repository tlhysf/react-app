import React from 'react';
import { StyleSheet, 
        Text, 
        TextInput, 
        View, 
        Button,
        Image, 
        BackHandler,
        /*TouchableOpacity*/ } from 'react-native';
import Firebase from 'firebase';
// import PasswordTextBox from './PasswordTextBox';

import { db } from '../Firebase/config';

export default class SignUp extends React.Component {
    state = { username: '', email: '', password: '', code: '', isAdmin: false, errorMessage: null }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.backHandler.remove();
    }
    
    handleBackButtonClick = () => {
        this.props.navigation.navigate('Login');
        // this.goBack();
        return true;
    }

    handleSignUp = () => {
        Firebase.auth().createUserWithEmailAndPassword(this.state.email.trim(),this.state.password)
        .then((user) => {
            Firebase.database().ref('/users/' + user.user.uid ).set({
                Name: this.state.username,
                email: this.state.email,
                code: this.state.code,
                // password: password,
                isAdmin: this.state.isAdmin,
            })
        })
        .then( () => this.props.navigation.navigate('Login'))
        .catch(error => this.setState({errorMessage: error.message}))
    }

    render() {
        return (
            <View style = {styles.container}>
                <Image source={require('C:/Users/care111/Desktop/REACT-NATIVE PROJECTS/iotsecurity/images/atomic-energy.png')} />
                <Text style = {styles.signupstyle}>Sign Up</Text>
                    {this.state.errorMessage && 
                        <Text style={{color: 'red'}}>
                            {this.state.errorMessage}
                        </Text>}
                    <TextInput placeholder='Full Name' autoCapitalize='none' style={styles.textInput}
                    onChangeText={username => this.setState({username})} value={this.state.username}/>
                    <TextInput placeholder='E-mail' autoCapitalize='none' autoCorrect={false} style={styles.textInput} 
                    onChangeText={email => this.setState({email})} value={this.state.email}/>
                    <TextInput secureTextEntry placeholder='Password' autoCapitalize='none' autoCorrect={false} style={styles.textInput}
                    onChangeText={password => this.setState({password})} value={this.state.password}/> 
                    <TextInput placeholder='Assigned Code' autoCapitalize='none' style={styles.textInput}
                    onChangeText={username => this.setState({code})} value={this.state.code}/>
                    {/* <PasswordTextBox icon='lock' placeholder='Password' style={styles.textInput} 
                    onChangeText={ password => this.setState({password}) } value={this.state.password}/> */}
                    <Button title='Sign Up' color='#e93766' onPress = {this.handleSignUp} />
                    <View>
                        <Text> Already have an account? <Text onPress ={() => this.props.navigation.navigate('Login')} 
                        style={styles.alreadyaccount} > Login </Text> </Text>
                    </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
    },
    textInput: {
        height: 40,
        fontSize: 20,
        width: '90%',
        borderColor: '#0b9b9b',
        borderBottomWidth: 1,
        marginTop: 8,
        marginVertical: 15
    },
    signupstyle: {
        color: '#e93766',
        fontSize: 40
    },
    alreadyaccount: {
        color: '#e93766',
        fontSize: 18
    }
})