import React from 'react';
import { View, 
        Text, 
        ActivityIndicator, 
        StyleSheet } from 'react-native';

import Firebase from 'firebase';
import { db } from './Firebase/config';
///let itemsRef = db.ref('/users');

export default class Loading extends React.Component {
    componentDidMount() {
        Firebase.auth().onAuthStateChanged( user => {
            if(user != null) {
                Firebase.database().ref('/users/' + user.uid ).child('/isAdmin').on('value', snapshot => {
                    // let data = snapshot.val();
                    // let admincheck = Object.values(data);
                    // console.log(admincheck);
                    let isAdmin = snapshot.val();
                    if( isAdmin == false ){
                        console.log(isAdmin);
                        this.props.navigation.navigate('ViewPagerPage')
                        // alert('You are not an Admin.');
                    } else if( isAdmin == true ) {
                        console.log(isAdmin);
                        this.props.navigation.navigate('adminpage')
                    } else {
                        console.log(isAdmin);
                        Firebase.auth().signOut();
                        alert('Sorry Something went wrong, please try again.')
                    }
                })
            } else if(user == null) {
                this.props.navigation.navigate('Login');
            }
            // this.props.navigation.navigate(user ? 'ViewPagerPage' : 'Login')
        })
    }

    render() {
        return(
            <View style = {styles.container}>
                <Text style = {styles.textstyle}>Loading</Text>
                <ActivityIndicator color='#e93766' size='large'/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textstyle: {
        color: '#e93766',
        fontSize: 40
    }
})