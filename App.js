import React from 'react';
//import { StyleSheet, Text, View } from 'react-native';
import {createAppContainer, 
        createSwitchNavigator, /*createTab*/ } from 'react-navigation';
import Loading from './Loading';
import SignUp from './User/SignUp';
import Login from './User/Login';
import ForgotPassword from './User/ForgotPassword';
import adminlogin from './Admin/adminlogin';
import wificonfig from './Admin/wificonfig';
import adminpage from './Admin/adminpage';
import userwificonfig from './User/userwificonfig';
// import Drawer from './Drawer';
import Tabs from './Tabs';

const AppNavigator = createSwitchNavigator (
  {
    Tabs,
    SignUp,
    Login,
    Loading,
    ForgotPassword,
    adminlogin,
    wificonfig,
    adminpage,
    userwificonfig,
  },{
    initialRouteName: 'Loading'
  }
)

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer/>
    )
  }
}
