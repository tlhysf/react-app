import { Platform } from 'react-native';
import { createMaterialTopTabNavigator,
         createDrawerNavigator } from 'react-navigation';

import MyHomeScreen from './DrawerScreens/MyHomeScreen';
import NotificationsScreen from './DrawerScreens/NotificationsScreen';

import HomeScreen from './TabScreens/TemperatureNode';
import SettingsScreen from './TabScreens/Dashboard';
import DetailsScreen from './TabScreens/GasNode';

const TabNavigator = createMaterialTopTabNavigator({
    Home: HomeScreen,
    Settings: SettingsScreen,
    Details: DetailsScreen,
}, {
    tabBarOptions: {
        activeTintColor: '#8630dc',
        style: { marginTop: Platform.OS == 'ios' ? 20 : 24, backgroundColor: '#66d3d3' },
        inactiveTintColor: '#dc3071',
    },
});

const DrawerNavigator = createDrawerNavigator({
Home:{
   screen: MyHomeScreen,
},
Settings: {
   screen: NotificationsScreen,
},
});

