import React from 'react';
import { Image, Button, StyleSheet, View } from 'react-native';

class MyHomeScreen extends React.Component {
    static navigationOptions = {
      drawerLabel: 'Home',
      drawerIcon: ({ tintColor }) => (
        <Image
          source={require('../assets/icon.png')}
          style={[styles.icon, {tintColor: tintColor}]}
        />
      ),
    };
  
    render() {
      return (
        <View style = {{marginTop: 24}} >
        <Button
          onPress={() => this.props.navigation.navigate('NotificationsScreen')}
          title="Go to notifications"
        />
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    icon: {
      width: 24,
      height: 24,
    },
  });

  export default MyHomeScreen;