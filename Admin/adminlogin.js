import React from 'react';
import { StyleSheet, 
        Text, 
        TextInput, 
        View,
        Image, 
        Button,
        BackHandler } from 'react-native';
import Firebase from 'firebase';
// import PasswordTextBox from './PasswordTextBox';

export default class adminlogin extends React.Component{
    // constructor(props) {
    //     super(props)
    //     this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    // }
    
    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.backHandler.remove();
    }
    
    handleBackButtonClick = () => {
        this.props.navigation.navigate('Login');
        // this.goBack();
        return true;
    }

    state = { username: '', email: '', password: '', errorMessage: null }
    handleAdminLogin = () => {
        Firebase.auth().signInWithEmailAndPassword(this.state.email.trim(), this.state.password)
        .then((user) => {
            Firebase.database().ref('/users/' + user.user.uid ).child('/isAdmin').on('value', snapshot => {
                let isAdmin = snapshot.val();
                if( isAdmin == true ){
                    console.log(isAdmin);
                    this.props.navigation.navigate('adminpage');
                } else if( isAdmin == false ) {
                    console.log(isAdmin);
                    Firebase.auth().signOut();
                    alert('You are not an Admin.');
                } else {
                    console.log(isAdmin);
                    Firebase.auth().signOut();
                    alert('Sorry Something went wrong, please try again.')
                }
            })
        }) 
        .catch(error => this.setState({ errorMessage: error.message}))
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('C:/Users/care111/Desktop/REACT-NATIVE PROJECTS/iotsecurity/images/atomic-energy.png')} />
                <Text style={styles.loginstyle}>Admin Login</Text>
                    {this.state.errorMessage && 
                        <Text style={{color: 'red'}}>
                            {this.state.errorMessage}
                        </Text>}
                <TextInput style={styles.textInput} autoCapitalize='none' autoCorrect={false} placeholder='E-mail' 
                onChangeText={ email => this.setState({email}) } value={this.state.email} />
                <TextInput secureTextEntry style={styles.textInput} autoCapitalize='none' autoCorrect={false} placeholder='Password' 
                onChangeText={ password => this.setState({password}) } value={this.state.password} />
                {/* <PasswordTextBox icon='lock' placeholder='Password' style={styles.textInput} 
                onChangeText={ password => this.setState({password}) } value={this.state.password}/> */}
                <Button title='Login' color='#e93766' onPress={ this.handleAdminLogin /*adminpage*/} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        height: 40,
        fontSize: 20,
        width: '90%',
        borderColor: '#9b9b9b',
        borderBottomWidth: 1,
        marginTop: 8,
        marginVertical: 15
    },
    loginstyle: {
        color: '#e93766',
        fontSize: 40
    }
})