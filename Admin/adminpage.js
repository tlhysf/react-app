import React from 'react';
import { StyleSheet, 
        Text, 
        View,
        Button,
        Platform,
        Alert,
        BackHandler } from 'react-native';
import Firebase from 'firebase';


export default class adminpage extends React.Component {
    constructor(props) {
        super(props)
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        Alert.alert(
          'Logout',
          'Are you sure you want to logout?',
          [
            // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {text: 'Yes', onPress: () => Firebase.auth().signOut() },
          ],
          {cancelable: false},
        );
        // this.goBack();
        return true;
    }
    
    // ClickBackButtonYes() {
    //     Firebase.auth().signOut();
    //     this.props.navigation.navigate('adminlogin');
    //     // this.goBack();
    //     // return true;
    // }

    render() {
        return(
            <View style = {styles.container}>
                <Text> ... Wifi Configuration ...</Text>
                <Button title='Configure Wifi' onPress={() => this.props.navigation.navigate('wificonfig')}></Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        marginTop: (Platform.OS) === 'ios' ? 20 : 24
    }
})