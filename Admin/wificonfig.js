import React from 'react';
import { StyleSheet, Platform, WebView, BackHandler } from 'react-native';
// import * as Permissions from 'expo-permissions';

// async function alertIfLocationAsync() {
//   const { status } = await Permissions.getAsync(Permissions.LOCATION);
//   if (status !== 'granted') {
//     alert('Hey! You might want to enable Location for my app, it is good.');
//   } else {
//     alert('Hey! Thanks for the location access.');
//   }
// }

// async function getLocationAsync() {
//   // permissions returns only for location permissions on iOS and under certain conditions, see Permissions.LOCATION
//   const { status, permissions } = await Permissions.askAsync(Permissions.LOCATION);
//   if (status === 'granted') {
//     return Location.getCurrentPositionAsync({ enableHighAccuracy: true });
//   } else {
//     throw new Error('Location permission not granted');
//   }
// }

export default class wificonfig extends React.Component {
//   async componentDidMount() {
//     await alertIfLocationAsync();
//     await getLocationAsync();
//   }

constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
}

componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
}

componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
}

handleBackButtonClick() {
    this.props.navigation.navigate('adminpage');
    // this.goBack();
    return true;
}

  render() {
    return (
        <WebView 
        style={styles.WebViewStyle} 
        source={{uri: 'http://192.168.4.1'}} 
        javaScriptEnabled={true}
        domStorageEnabled={true}  />
    );
  }
}

const styles = StyleSheet.create({
  WebViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:  (Platform.OS) === 'ios' ? 20 : 24
  }
});