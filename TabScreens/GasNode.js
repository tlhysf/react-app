import React from 'react';
import {  View, 
        StyleSheet, 
        Text, 
        Alert,
        Platform,
        BackHandler,
        Dimensions,
        Button} from 'react-native';    

import Firebase from 'firebase';
import { db } from '../Firebase/config';

let itemsRef2 = db.ref('/Gas');

const screenwidth = Math.round(Dimensions.get('window').width);
const screenheight = Math.round(Dimensions.get('window').height);

let dec = 'Detected';

class GasNode extends React.Component {
    state = {
        items2: [],
        currentUser: null
    };

    handleLogout = () => {
        Alert.alert(
            'Logout',
            'Are you sure you want to logout?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => Firebase.auth().signOut()},
            ],
            {cancelable: false},
        );
    };

    componentDidMount() {
        const {currentUser} = Firebase.auth();
        this.setState({currentUser});
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        
        itemsRef2.orderByChild('dateAdded').limitToLast(1).on('value', snapshot => {
            let data = snapshot.val();
            let items2 = Object.values(data);
            // if( items2 == 'Detected') {
            //     this.sendGasNotification();
            // }
            this.setState({items2});
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        Alert.alert('','Please use the logout button.')
        // this.goBack();
        return true;
    }

    render() {
        return (    
            <View style={styles.container}>
                <Text style = {styles.textheading}>Gas Node</Text>
                <Text style = {styles.messageinput}> Gas: </Text>
                    {this.state.items2.length > 0 ? (
                        <Text style = {styles.textinput}>{items5 = this.state.items2} </Text>
                    ) : (
                        <Text style = {styles.textinput}>No Data</Text>
                    )}
                <View style = {{justifyContent: 'center', alignItems: 'center'}}>
                    {this.state.items2 == dec ? (
                        <View style = {styles.CircleShapeView} />
                    ) : (
                        <View style = {styles.CircleShapeView1} />
                    )}
                </View>
                <Button color='#e93766' title='Log out' onPress={this.handleLogout}></Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: Platform.OS == 'ios' ? 20 : 24, 
        flex: 1,
        //justifyContent: 'center',
        backgroundColor: '#FFFFFF'
    },
    textheading: {
        textAlign: 'center',
        //marginTop: 30,
        color: '#FFFFFF',
        backgroundColor: '#e93766',
        fontWeight: 'bold',
        fontSize: 0.04*screenheight,
        alignItems: 'baseline',
        justifyContent: 'flex-start'
    },
    messageinput: {
        marginTop: 0.35*screenheight,
        fontSize: 0.04*screenheight,
        justifyContent: 'center',
        color: '#e93766'
    },
    textinput: {
        height: 0.06*screenheight,
        padding: 4,
        marginBottom: 0.065*screenheight,
        fontSize: 0.03*screenheight,
        borderWidth: 1,
        justifyContent: 'center',
        textAlign: 'center',
        borderColor: '#e93766',
        borderRadius: 0.03*screenheight,
        color: '#e93766'
    },
    CircleShapeView: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 100,
        borderRadius: 100/2,
        backgroundColor: '#00BCD4',
        marginBottom: 0.10*screenheight
    },
    CircleShapeView1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 100,
        borderRadius: 100/2,
        backgroundColor: '#D3D3D3',
        marginBottom: 0.10*screenheight
    }
});

export default GasNode;