import React from 'react';
import { Text,
        StyleSheet,
        Button,
        Alert,
        Platform,
        BackHandler,
        Dimensions,
        View } from 'react-native';
import {LineChart,
        YAxis,
        //XAxis,
        Grid } from 'react-native-svg-charts';
import {GaugeProgress,
        /*AnimatedGaugeProgress*/ } from 'react-native-simple-gauge'; 
import Firebase from 'firebase';  
import { db } from '../Firebase/config';

let itemsRef = db.ref('/logDHT/Temperature');    

const screenwidth = Math.round(Dimensions.get('window').width);
const screenheight = Math.round(Dimensions.get('window').height);

const size1 = 0.35*screenheight;
const width1 = 15;
const textOffset = 15;
const cropDegree = 90;
const textWidth1 = size1 - (textOffset*2);
const textHeight1 = size1*(1- cropDegree/360) - (textOffset*2);

class TemperatureNode extends React.Component {

  state = {
    items: [],
    items1: [],
    currentUser: null
  };

  handleLogout = () => {
    Alert.alert(
        'Logout',
        'Are you sure you want to logout?',
        [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () => Firebase.auth().signOut()},
        ],
        {cancelable: false},
    );
  };

  componentDidMount() {
    const {currentUser} = Firebase.auth();
    this.setState({currentUser});
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    
    itemsRef.orderByChild("dateAdded").limitToLast(100).on('value', snapshot => {
        let data = snapshot.val();
        let items = Object.values(data);
        this.setState({items});
    });
    itemsRef.orderByChild("dateAdded").limitToLast(1).on('value', snapshot => {
        let data = snapshot.val();
        let items1 = Object.values(data);
        // if( items1 == 'Detected') {
        //     this.sendTempNotification();
        // }
        this.setState({items1});
    });
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
      Alert.alert('','Please use the logout button.')
      // this.goBack();
      return true;
  }

  render() {
    return (
      <View style={styles.container}>
          <Text style = {styles.textheading }>Temperature Node</Text>
          <Text style = { styles.textinput1 }>Temperature Chart</Text>
          <View style={styles.chartstyle}>
              <YAxis data={ this.state.items.map(parseFloat) } contentInset={{top: 0.02*screenheight, bottom: 0.02*screenheight}}//, right: 0.05*screenwidth, left: 0.05*screenwidth}}
              svg={{ fill: 'grey', fontSize: 0.01*screenheight,}} numberOfTicks={ 10 } formatLabel={ value => `${value}ºC` } />
              {/* <View style = {{flexDirection:'column'}}> */}
                  <LineChart style = {{ width: '90%' }} data = { this.state.items.map(parseFloat) } 
                  svg = {{stroke: 'rgb(134, 65, 244)'}} contentInset = {{top: 0.02*screenheight, bottom: 0.02*screenheight, right: 0.05*screenwidth, left: 0.05*screenwidth}}>
                      <Grid/>
                  </LineChart>
                  {/* <XAxis style={{ marginHorizontal: -10 }} data={ date }  formatLabel={ (value, index) => index } 
                  contentInset={{ right: 0.05*screenwidth, left: 0.05*screenwidth }} svg={{ fontSize: 10, fill: 'black' }}></XAxis> */}
              {/* </View> */}
          </View>
          <Text style = { styles.textinput2 }>Temperature Gauge</Text>
          <GaugeProgress style = {styles.gaugestyle} size={size1} width={width1} fill={ this.state.items1 } tintColor="#4682b4" delay={0} 
          cropDegree={cropDegree} textOffset={textOffset} textWidth= {textWidth1} textHeight = {textHeight1}>
              <View style = {styles.textView}>
                  <Text style = {styles.text}>{ items1 = this.state.items1 } °C</Text>
              </View>
          </GaugeProgress>
          <View style={{marginTop: 5}}>
              <Button /*style={{size='25%'}}*/ color='#e93766' title='Log out' onPress={this.handleLogout}></Button>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS == 'ios' ? 20 : 24, 
    flex: 1,
    //justifyContent: 'center',
    backgroundColor: '#FFFFFF'
  },
  textheading: {
    textAlign: 'center',
    //marginTop: 30,
    color: '#FFFFFF',
    backgroundColor: '#e93766',
    fontWeight: 'bold',
    fontSize: 0.04*screenheight,
    alignItems: 'baseline',
    justifyContent: 'flex-start'
  },
  textinput1: {
    fontSize: 0.02*screenheight,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent : 'center',
    fontWeight: 'bold',
    color: '#e93766',
    marginTop: 0.03*screenheight
  },
  chartstyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: '30%',
    //marginTop: 10,
    backgroundColor: '#FFFFFF', 
    color: '#e93766'
  },
  textinput2: {
    fontSize: 0.02*screenheight,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent : 'center',
    fontWeight: 'bold',
    color: '#e93766',
    marginTop: 0.01*screenheight,
    marginBottom: 0.03*screenheight
  },
  gaugestyle: {
    //justifyContent: 'center', 
    alignItems: 'center'
    //marginBottom: 0.025*screenheight
  },
  textView: {
    position: 'absolute',
    top: textOffset,
    left: textOffset,
    width: textWidth1,
    height: textHeight1,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: '#e93766',
    fontSize: 25,
    textAlign: 'center',
    marginLeft: 0.33*screenwidth
  }
});

export default TemperatureNode;