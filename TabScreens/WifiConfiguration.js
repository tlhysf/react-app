import React from 'react';
import { View, 
        StyleSheet, 
        Text, 
        Platform,
        BackHandler,
        Button} from 'react-native';    

class WifiConfiguration extends React.Component {
    componentWillMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        Alert.alert('','Please use the logout button.')
        // this.goBack();
        return true;
    }

    render() {
        return (    
            <View style = {styles.container1}>
                <Text> ... Wifi Configuration ...</Text>
                <Button title='Configure Wifi' onPress={() => this.props.navigation.navigate('userwificonfig')}></Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container1: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        marginTop: (Platform.OS) === 'ios' ? 20 : 24
    }
});

export default WifiConfiguration;