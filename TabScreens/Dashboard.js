import React from 'react';
import { StyleSheet, 
        View, 
        Text, 
        Button,
        Alert,
        Platform,
        BackHandler,
        Switch,
        Dimensions } from 'react-native';
import { Notifications, Permissions } from 'expo';
import {GaugeProgress,
        /*AnimatedGaugeProgress*/ } from 'react-native-simple-gauge';
import Firebase from 'firebase';
import { db } from '../Firebase/config';

let itemsRef = db.ref('/logDHT/Temperature');
let itemsRef2 = db.ref('/Gas');
let itemsRef3 = db.ref('/Motion');
let itemsRef4 = db.ref('/Vibration');
let itemsRef5 = db.ref('/Magnetic');
let itemsRef6 = db.ref('/logDHT/Humidity');

const screenwidth = Math.round(Dimensions.get('window').width);
const screenheight = Math.round(Dimensions.get('window').height);

const size = 0.20*screenheight;
const width = 10;
const textOffset = 15;
const cropDegree = 90;
const textWidth = size - (textOffset*2);
const textHeight = size*(1- cropDegree/360) - (textOffset*2);

let dec = 'Detected';

class Dashboard extends React.Component {
    state = {
        items1: [],
        items2: [],
        items3: [],
        items4: [],
        items5: [],
        items6: [],
        currentUser: null,
        switchvalue: false
    };

    handleToggleSwitch = () => 
    this.setState( state => ({
        switchvalue: !state.switchvalue,
    }));

    handleLogout = () => {
        Alert.alert(
            'Logout',
            'Are you sure you want to logout?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => Firebase.auth().signOut()},
            ],
            {cancelable: false},
        );
    };

    askPermissions = async () => {
        const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;
        if (existingStatus !== granted) {
          const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
          finalStatus = status;
        }
        if (finalStatus !== granted) {
          return false;
        }
        return true;
    };

    sendDoorNotification = async () => {
        if( this.state.switchvalue == true ) {
            let notificationId = await Notifications.presentLocalNotificationAsync({
                title: 'Door Sensor',
                body: 'Door is Open.',
                
            });
            console.log(notificationId); // can be saved in AsyncStorage or send to server
        }
        
    };

    sendTempNotification = async () => {
        if( this.state.switchvalue == true ) {
            let notificationId = await Notifications.presentLocalNotificationAsync({
                title: 'Temperature Sensor',
                body: 'Temperature is reaching Dangerous Levels.',
                
            });
            console.log(notificationId); // can be saved in AsyncStorage or send to server
        }
    };

    sendGasNotification = async () => {
        if( this.state.switchvalue == true ) {
            let notificationId = await Notifications.presentLocalNotificationAsync({
                title: 'Gas Sensor',
                body: 'Gas is being DETECTED.',
                
            });
            console.log(notificationId); // can be saved in AsyncStorage or send to server
        }
    };

    sendMotionNotification = async () => {
        if( this.state.switchvalue == true ) {
            let notificationId = await Notifications.presentLocalNotificationAsync({
                title: 'Motion Sensor',
                body: 'Motion is being DETECTED.',
                
            });
            console.log(notificationId); // can be saved in AsyncStorage or send to server
        }
    };

    sendVibrationNotification = async () => {
        if( this.state.switchvalue == true ) {
            let notificationId = await Notifications.presentLocalNotificationAsync({
                title: 'Vibration Sensor',
                body: 'Your windows/door is vibrating, there maybe an Earthquake.',
                
            });
            console.log(notificationId); // can be saved in AsyncStorage or send to server
        }
    };

    componentDidMount() {
        const {currentUser} = Firebase.auth();
        this.setState({currentUser});
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.askPermissions();
        
        itemsRef.orderByChild("dateAdded").limitToLast(1).on('value', snapshot => {
            let data = snapshot.val();
            let items1 = Object.values(data);
            if( items1 == 'Detected') {
                this.sendTempNotification();
            }
            this.setState({items1});
        });
        itemsRef2.orderByChild('dateAdded').limitToLast(1).on('value', snapshot => {
            let data = snapshot.val();
            let items2 = Object.values(data);
            if( items2 == 'Detected') {
                this.sendGasNotification();
            }
            this.setState({items2});
        });
        itemsRef3.orderByChild('dateAdded').limitToLast(1).on('value', snapshot => {
            let data = snapshot.val();
            let items3 = Object.values(data);
            if( items3 == 'Detected') {
                this.sendMotionNotification();
            }
            this.setState({items3});
        });
        itemsRef4.orderByChild('dateAdded').limitToLast(1).on('value', snapshot => {
                let data = snapshot.val();
                let items4 = Object.values(data);
                if( items4 == 'Detected') {
                    this.sendVibrationNotification();
                }
                this.setState({items4});
        });
        itemsRef5.orderByChild('dateAdded').limitToLast(1).on('value', snapshot => {
            let data = snapshot.val();
            let items5 = Object.values(data);
            if( items5 == 'Detected') {
                this.sendDoorNotification();
            }
            this.setState({items5});
        });
        itemsRef6.orderByChild('dateAdded').limitToLast(1).on('value', snapshot => {
            let data = snapshot.val();
            let items6 = Object.values(data);
            this.setState({items6});
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        Alert.alert('','Please use the logout button.')
        // this.goBack();
        return true;
    }

    render() {
      return (
        <View style={styles.container}>
            <Text style = {styles.textheading}>DASHBOARD</Text>
            <View style = {{marginTop:10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                <Text style={{ fontSize:20, fontWeight:'bold', marginRight:15, color:'#87CEFA' }}> Arm/Disarm </Text>
                <Switch style={{ marginLeft:15, transform: [{scaleX: 1.5}, {scaleY: 1.5}] }} onValueChange={this.handleToggleSwitch} 
                value={this.state.switchvalue} />
            </View>
            <View style={{ marginTop:10, justifyContent:'space-evenly', flexDirection:'row' }}>
            <View style={{flexDirection:'column'}}>
                    <Text style = { styles.textinput2 }>Temperature</Text>
                    <GaugeProgress style = {styles.gaugestyle} size={size} width={width} fill={ this.state.items1 } tintColor="#4682b4" delay={0} 
                    cropDegree={cropDegree} textOffset={textOffset} textWidth= {textWidth} textHeight = {textHeight}>
                        <View style = {styles.textView1}>
                            <Text style = {styles.text1}>{ items1 = this.state.items1 } °C</Text>
                        </View>
                    </GaugeProgress>
                </View>
                <View style={{flexDirection:'column'}}>
                    <Text style = { styles.textinput2 }>Humidity</Text>
                    <GaugeProgress style = {styles.gaugestyle} size={size} width={width} fill={ this.state.items6 } tintColor="#4682b4" delay={0} 
                    cropDegree={cropDegree} textOffset={textOffset} textWidth= {textWidth} textHeight = {textHeight}>
                        <View style = {styles.textView1}>
                            <Text style = {styles.text1}>{ items6 = this.state.items6 } %</Text>
                        </View>
                    </GaugeProgress>
                </View>
            </View>
            <View style={{justifyContent:'center', marginTop: 20, flexDirection: 'row'}}>
                <View style={{justifyContent:'center', flexDirection:'column'}} >
                    <Text style = {styles.messageinput1}> Motion </Text>
                    {this.state.items3 == dec ? (
                        <View style = {styles.CircleShapeView2} />
                    ) : (
                        <View style = {styles.CircleShapeView3} />
                    )}
                </View>
                <View style={{marginVertical: 20, marginHorizontal: 20}} />
                <View style={{justifyContent:'center', flexDirection:'column'}} >
                    <Text style = {styles.messageinput1}> Gas </Text>
                    {this.state.items2 == dec ? (
                        <View style = {styles.CircleShapeView2} />
                    ) : (
                        <View style = {styles.CircleShapeView3} />
                    )}
                </View>
            </View>
            <View style={{justifyContent:'center', marginTop:10, flexDirection: 'row'}}>
                <View style={{justifyContent:'center', flexDirection:'column'}} >
                    <Text style = {styles.messageinput1}> Vibration </Text>
                    {this.state.items4 == dec ? (
                        <View style = {styles.CircleShapeView2} />
                    ) : (
                        <View style = {styles.CircleShapeView3} />
                    )}
                </View>
                <View style={{marginVertical: 25, marginHorizontal: 10}} />
                <View style={{justifyContent:'center', flexDirection:'column'}} >
                    <Text style = {styles.messageinput1}> Door </Text>
                    {this.state.items5 == dec ? (
                        <View style = {styles.CircleShapeView2} />
                    ) : (
                        <View style = {styles.CircleShapeView3} />
                    )}
                </View>
            </View>
            <View style={{ marginTop: 16 }}>
                <Button color='#e93766' title='Log out' onPress={this.handleLogout}></Button>
            </View>
        </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS == 'ios' ? 20 : 24, 
    flex: 1,
    //justifyContent: 'center',
    backgroundColor: '#FFFFFF'
  },
  textheading: {
    textAlign: 'center',
    //marginTop: 30,
    color: '#FFFFFF',
    backgroundColor: '#e93766',
    fontWeight: 'bold',
    fontSize: 0.04*screenheight,
    alignItems: 'baseline',
    justifyContent: 'flex-start'
  },
  textinput2: {
    fontSize: 0.02*screenheight,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent : 'center',
    fontWeight: 'bold',
    color: '#e93766',
    marginTop: 0.01*screenheight,
    marginBottom: 0.03*screenheight
  },
  gaugestyle: {
    //justifyContent: 'center', 
    alignItems: 'center'
    //marginBottom: 0.025*screenheight
  },
  textView1: {
    position: 'absolute',
    top: textOffset,
    left: textOffset,
    width: textWidth,
    height: textHeight,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text1: {
    color: '#e93766',
    fontSize: 20,
    textAlign: 'center',
    marginLeft: 0.022*screenwidth
  },
  messageinput1: {
    //marginTop: 0.35*screenheight,
    fontSize: 0.04*screenheight,
    justifyContent: 'space-evenly',
    color: '#e93766'
  },
  CircleShapeView2: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 100,
    borderRadius: 100/2,
    backgroundColor: '#00BCD4'
},
CircleShapeView3: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 100,
    borderRadius: 100/2,
    backgroundColor: '#D3D3D3'
  }
});

export default Dashboard;